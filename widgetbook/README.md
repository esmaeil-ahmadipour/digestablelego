# widgetbook

Mono-repo widget library that uses packages:

* Animation: Cool animation techniques.

Packages that can be added later:

* Accessiblity - Make the app accessible.
* Assets - Manage assets, display images, and show icons.
* Form - Form widgets used to capture user input.
* Layouts - Screen layouts.
* Motion - Scrolling (Parallax) and effects.
* Painting - These widgets apply visual effects to the children without changing their layout, size, or position.
* Stylin - Branding and appearance, typography, logo's etc...

https://docs.flutter.dev/development/ui/widgets
https://medium.com/@simbu/flutter-packages-3b0beea520e0

cmd:

flutter create --template=package <PackageName>