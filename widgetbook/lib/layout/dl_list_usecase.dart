// Flutter imports:
import 'package:asset_lego/dl_avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:layout_lego/dl_list.dart';
import 'package:layout_lego/models/dl_list_item.dart';
import 'package:widgetbook/widgetbook.dart' show Knobs, Option;//, Option;

// Package imports:
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@WidgetbookUseCase(name: 'Title Only', type: DlList)
Widget dlListTitleOnly(BuildContext context) {
  return buildItem(DlListItem(
      title: Text(context.knobs.text(label: 'Title', initialValue: "Title"))));
}

@WidgetbookUseCase(name: 'Title and Subtitle', type: DlList)
Widget dlListTitleAndSubtitle(BuildContext context) {
  return buildItem(DlListItem(
    title: Text(context.knobs.text(label: 'Title', initialValue: "Title")),
    subtitle:
        Text(context.knobs.text(label: 'SubTitle', initialValue: "SubTitle")),
  ));
}

@WidgetbookUseCase(name: 'Logo, Title and Subtitle', type: DlList)
Widget dlListLogoTitleAndSubtitle(BuildContext context) {
  return buildItem(DlListItem(
    leading: imageOptionsKnob(context),
    title: Text(context.knobs.text(label: 'Title', initialValue: "Title")),
    subtitle:
        Text(context.knobs.text(label: 'SubTitle', initialValue: "SubTitle")),
  ));
}

@WidgetbookUseCase(name: 'Avatar, Title and Subtitle', type: DlList)
Widget dlListAvatarTitleAndSubtitle(BuildContext context) {
  return buildItem(DlListItem(
    leading: DlAvatar(
      imageOptionsKnob(context),
      radius: 22,
    ),
    title: Text(context.knobs.text(label: 'Title', initialValue: "Title")),
    subtitle:
        Text(context.knobs.text(label: 'SubTitle', initialValue: "SubTitle")),
  ));
}

@WidgetbookUseCase(name: 'Example', type: DlList)
Widget dlListExample(BuildContext context) {
  return build(
    DlList(
      exampleListItems,
      margin: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 60),
    )
  );
}

Image imageOptionsKnob(BuildContext context) {
  return context.knobs.options(
    label: 'Logo',
    options: [
      Option<Image>(
        label: 'Flutter logo',
        value: Image.asset("assets/imgs/flutter.png"),
      ),
      Option(
        label: 'Widgetbook logo',
        value: Image.asset("assets/imgs/widgetbook_logo.jpeg"),
      )
    ],
  );
}

Widget build(Widget child) {
  return CupertinoPageScaffold(
    navigationBar: const CupertinoNavigationBar(
      middle: Text("Example List Items"),
    ),
    child: child,
  );
}

Widget buildList(List<DlListItem> listItems) {
  return DlList(
    listItems,
    margin: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 60),
  );
}

Widget buildItem(DlListItem listItems) {
  return build(buildList([listItems]));
}

List<DlListItem> get exampleListItems {
    return [
      DlListItem(title: const Text("Title")),
      DlListItem(
        title: const Text("Title"),
        subtitle: const Text("Sub-Title"),
      ),
      DlListItem(
          title: const Text("Title"),
          subtitle: const Text("No right side navigation icon '>'"),
          trailing: SizedBox(
            width: 1,
            child: Container(),
          )),
      DlListItem(
          title: const Text("Title"),
          subtitle: const Text(
              "ThreeLines: Set isThreeLines to true when the subtitle is longer and wraps and you want more space to display it."),
          trailing: SizedBox(
            width: 1,
            child: Container(),
          ),
          isThreeLine: true),
      DlListItem(
        leading: const FlutterLogo(),
        title: const Text("Logo"),
      ),
      DlListItem(
        leading: const FlutterLogo(),
        title: const Text("Logo"),
        subtitle: const Text("Sub-Title"),
      ),
      DlListItem(
          leading: const FlutterLogo(),
          title: const Text("Logo"),
          subtitle: const Text("No right side navigation icon '>'"),
          trailing: SizedBox(
            width: 1,
            child: Container(),
          )),
      DlListItem(
          leading: const FlutterLogo(),
          title: const Text("Logo"),
          subtitle: const Text(
              "ThreeLines: Set isThreeLines to true when the subtitle is longer and wraps and you want more space to display it."),
          trailing: SizedBox(
            width: 1,
            child: Container(),
          ),
          isThreeLine: true),
      DlListItem(
        leading: DlAvatar(
          Image.asset("assets/imgs/flutter.png"),
          radius: 22,
        ),
        title: const Text("As Avatar"),
      ),
      DlListItem(
        title: const Text("Another Item"),
      ),
      DlListItem(
        title: const Text("One more"),
      ),
      DlListItem(
        title: const Text("..."),
      ),
    ];
  }
