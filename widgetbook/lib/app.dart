//import 'package:animation/animation.dart';
import 'package:flutter/material.dart';
import 'package:widgetbook_annotation/widgetbook_annotation.dart';

@WidgetbookTheme(name: 'Light')
ThemeData lightTheme() {
  return ThemeData.light();
}

@WidgetbookTheme(name: 'Dark')
ThemeData darkTheme() {
  return ThemeData.dark();
}

@WidgetbookApp.material(
  name: 'Widgets',
  textScaleFactors: [1, 1.5, 2],
  devices: [
    Apple.iPhone12,
    Apple.iPhone13,
    Apple.iPadMini,
    Desktop.desktop1080p,
  ],
)
int? notUsed;
