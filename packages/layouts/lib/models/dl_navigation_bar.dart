/*
  NavItem - A navigation item used to supply content to the Navigation bars used in the application.

  ✓ NavItem:  Has a leading widget for navigation controls like back '<'.
  ✓ NavItem:  Has a middle widget to display contextual information like a title 'Recipes'
  ✓ NavItem:  Has a trailing widget for actions like add '+'
  ✓ NavItem:  Has a hero tag to enable transition animations.

*/

// Flutter imports:
import 'package:flutter/widgets.dart';

class DlNavigationBar {
  static Widget defaultWidget = Container();
  final Widget? leading;
  final Widget? actions;
  final Object heroTag;
  final String title;

  const DlNavigationBar({
    this.leading,
    this.actions,
    required this.title,
    this.heroTag = "",
  });
}

