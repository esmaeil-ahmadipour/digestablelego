// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';

class DlListItem {
  final Widget? leading;
  final Widget? trailing;
  final Widget? title;
  final Widget? subtitle;
  final GestureTapCallback? onTap;
  final bool? isThreeLine;

  DlListItem({
    this.leading,
    this.trailing,
    this.title,
    this.subtitle,
    this.onTap, 
    this.isThreeLine,
  });
}
