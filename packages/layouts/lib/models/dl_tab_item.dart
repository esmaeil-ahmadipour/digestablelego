// Flutter imports:
import 'package:flutter/widgets.dart';
import 'package:layout_lego/models/dl_navigation_bar.dart';

class DlTabItem with ChangeNotifier {
  // The icon that is displayed
  final Icon icon;

  // The label that is displayed below the icon. Optional on Apple devices, required by material design.
  final String? label;

  // The content displayed when the tab is selected.
  final Widget content;

  // The global key used for navigation.
  final GlobalKey<NavigatorState> globalKey;

  // The navigation bar values.
  final DlNavigationBar? navigationBar;

  DlTabItem({
    required this.icon,
    this.label,
    required this.content,
    required this.globalKey,
    required this.navigationBar,
  });

  @override
  String toString() => 'TabItem(label: $label, icon: $icon)';
}

extension DlTabItems on List<DlTabItem> {
  BottomNavigationBarItem buildBottomNavBarItem(int index) {
    return BottomNavigationBarItem(
      icon: this[index].icon,
      label: this[index].label,
    );
  }
}
