// Flutter imports:
import 'package:flutter/widgets.dart';

/// Scrolling container
class DlScrollable extends StatelessWidget {
  const DlScrollable({
    Key? key,
    required this.items,
  }) : super(key: key);

  final List<Widget> items;

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(slivers: <Widget>[
      SliverSafeArea(
        top: false,
        minimum: const EdgeInsets.only(top: 4),
        sliver: SliverList(
          delegate: SliverChildListDelegate(
            [
              for(var item in items) item
            ],
          ),
        ),
      ),
    ]);
  }
}
