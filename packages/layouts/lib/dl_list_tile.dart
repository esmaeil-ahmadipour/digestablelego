// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:layout_lego/models/dl_list_item.dart';

// Project imports:
import 'package:simbucore/theme/extension/buildcontext_extension.dart';

/// List item
class DlListTile extends StatelessWidget {
  final DlListItem listItem;
  final Border? tileBorder;
  const DlListTile(this.listItem, {Key? key, this.tileBorder})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return context.isCupertinoApp()
        ? CupertinoListTile(
            leading: listItem.leading,
            trailing: listItem.trailing,
            title: listItem.title ?? const Text(""),
            subtitle: listItem.subtitle,
            onTap: listItem.onTap,
          )
        : ListTile(
            leading: listItem.leading,
            trailing: listItem.trailing,
            title: listItem.title,
            subtitle: listItem.subtitle,
            onTap: listItem.onTap,
            isThreeLine: listItem.isThreeLine ?? false,
          );
  }

  // ShapeBorder _cupertinoBorder(BuildContext context) {
  //   return tileBorder ??
  //       Border(
  //         bottom: BorderSide(
  //           color: CupertinoDynamicColor.resolve(
  //               CupertinoColors.separator, context),
  //           width: 0.8
  //         ),
  //       );
  // }
}