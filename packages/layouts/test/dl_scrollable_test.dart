/*
Tabbed Scaffold - Scaffold used to display content in a tab.

Done
Creates the scaffold
Uses the safe area
Accepts a child widget to display the content
Accepts a navigation bar
*/

// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:layout_lego/dl_scrollable.dart';

void main() {
  group('Scrollable Content: ', () {
    isScrollable();
  });
}


Future<void> isScrollable() async {
  return testWidgets('The content is scrollable', (WidgetTester tester) async {
    var tabScaffold = CupertinoApp(
      home: DlScrollable(
        items: [
          for (var item in List<String>.generate(40, (i) => "Item $i"))
            Text(item)
        ],
      ),
    );
    // Would generate an overflow error if not scrollable.
    // A RenderFlex overflowed by 250 pixels on the bottom.
    await tester.pumpWidget(tabScaffold);
    expect(find.byType(Text), findsWidgets);
  });
}
