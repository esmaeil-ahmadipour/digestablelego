/*
  DmList - Well polished useful widget, brilliantly coded with care.
*/
// #     #                                 #######
// #  #  # # #####   ####  ###### #####       #    ######  ####  #####
// #  #  # # #    # #    # #        #         #    #      #        #
// #  #  # # #    # #      #####    #         #    #####   ####    #
// #  #  # # #    # #  ### #        #         #    #           #   #
// #  #  # # #    # #    # #        #         #    #      #    #   #
//  ## ##  # #####   ####  ######   #         #    ######  ####    #

// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:layout_lego/dl_list.dart';
import 'package:layout_lego/models/dl_list_item.dart';
import 'package:utils/fixture/widget_extension.dart';

//  ######
// #     # #    # #    #
// #     # #    # ##   #
// ######  #    # # #  #
// #   #   #    # #  # #
// #    #  #    # #   ##
// #     #  ####  #    #

void main() {
  group('DigestableLego, List: ', () {
    containerForListItems();

    displaysCupertinoListTilesForCupertinoApps();
    cupertinoListTilesCanDisplayLeadingWidget();
    cupertinoListTilesCanDisplayATitle();
    cupertinoListTilesCanDisplayASubTitle();
    cupertinoListTilesCanDisplayTrailingWidget();
    cupertinoListTilesCanDisplayTrailingNavigation();
    cupertinoListTilesCanSetOnTapFunction();

    displaysListTilesForMaterialApps();
    materialListTilesCanDisplayLeadingWidget();
    materialListTilesCanDisplayATitle();
    materialListTilesCanDisplayASubTitle();
    materialListTilesCanDisplayTrailingWidget();
    materialListTilesCanSetOnTapFunction();
  });
}

// #######
//    #    ######  ####  #####
//    #    #      #        #
//    #    #####   ####    #
//    #    #           #   #
//    #    #      #    #   #
//    #    ######  ####    #

GestureTapCallback? onTap = () => "Ouch!";

List<DlListItem> get listItems => <DlListItem>[
      DlListItem(
        leading: const Text("Leading"),
        trailing: const Text("Trailing"),
        title: const Text("Title"),
        subtitle: const Text("SubTitle"),
        onTap: onTap,
      ),
    ];

Future<void> containerForListItems() async {
  return testWidgets('Is a container for list items',
      (WidgetTester tester) async {
    await tester.pumpWidget(DlList(listItems).withCupertinoApp());
    expect(find.byType(ListView), findsOneWidget);
  });
}

Future<void> displaysListTilesForMaterialApps() async {
  return testWidgets('Each item is a ListTile in a MaterialApp.',
      (WidgetTester tester) async {
    var widget = Scaffold(body: DlList(listItems)).withCupertinoApp();
    await tester.pumpWidget(widget);
    expect(find.byType(ListTile), findsWidgets);
  });
}

Future<void> displaysCupertinoListTilesForCupertinoApps() async {
  return testWidgets('Each item is a CupertinoListTile in a CupertinoApp.',
      (WidgetTester tester) async {
    var widget = DlList(listItems).withCupertinoApp();
    await tester.pumpWidget(widget);
    expect(find.byType(CupertinoListTile), findsWidgets);
  });
}

// Leading tests

Future<void> materialListTilesCanDisplayLeadingWidget() async {
  return testWidgets(
      'Material ListTiles have a leading widget on the left, typically a logo or avatar.',
      (WidgetTester tester) async {
    var widget = Scaffold(body: DlList(listItems)).withMaterialApp();
    await tester.pumpWidget(widget);
    expect(find.text("Leading"), findsOneWidget);
  });
}

Future<void> cupertinoListTilesCanDisplayLeadingWidget() async {
  return testWidgets(
      'Cupertino ListTiles have a leading widget on the left, typically a logo or avatar.',
      (WidgetTester tester) async {
    var widget = DlList(listItems).withCupertinoApp();
    await tester.pumpWidget(widget);
    expect(find.text("Leading"), findsOneWidget);
  });
}

// Title tests

Future<void> materialListTilesCanDisplayATitle() async {
  return testWidgets(
      'Material ListTiles have a title to the left of the leading widget.',
      (WidgetTester tester) async {
    var widget = Scaffold(body: DlList(listItems)).withMaterialApp();
    await tester.pumpWidget(widget);
    expect(find.text("Title"), findsOneWidget);
  });
}

Future<void> cupertinoListTilesCanDisplayATitle() async {
  return testWidgets(
      'Cupertino ListTiles have a title to the left of the leading widget.',
      (WidgetTester tester) async {
    var widget = DlList(listItems).withCupertinoApp();
    await tester.pumpWidget(widget);
    expect(find.text("Title"), findsOneWidget);
  });
}

// Sub-Title tests

Future<void> materialListTilesCanDisplayASubTitle() async {
  return testWidgets('Material ListTiles have a sub title under the title.',
      (WidgetTester tester) async {
    var widget = Scaffold(body: DlList(listItems)).withMaterialApp();
    await tester.pumpWidget(widget);
    expect(find.text("SubTitle"), findsOneWidget);
  });
}

Future<void> cupertinoListTilesCanDisplayASubTitle() async {
  return testWidgets('Cupertino ListTiles have a sub title under the title.',
      (WidgetTester tester) async {
    var widget = DlList(listItems).withCupertinoApp();
    await tester.pumpWidget(widget);
    expect(find.text("SubTitle"), findsOneWidget);
  });
}

// Trailing tests

Future<void> materialListTilesCanDisplayTrailingWidget() async {
  return testWidgets(
      "Material ListTiles have a trailing widget on the right, typical used for actions like add '+', or badges.",
      (WidgetTester tester) async {
    var widget = Scaffold(body: DlList(listItems)).withMaterialApp();
    await tester.pumpWidget(widget);
    expect(find.text("Trailing"), findsOneWidget);
  });
}

Future<void> cupertinoListTilesCanDisplayTrailingWidget() async {
  return testWidgets(
      "Cupertino ListTiles have a trailing widget on the right, typical used for actions like add '+', or badges.",
      (WidgetTester tester) async {
    var widget = DlList(listItems).withCupertinoApp();
    await tester.pumpWidget(widget);
    expect(find.text("Trailing"), findsOneWidget);
  });
}

Future<void> cupertinoListTilesCanDisplayTrailingNavigation() async {
  return testWidgets(
      "Cupertino ListTiles show the trailing navigation symbol '>' on right by default. Supply a trailaing widget if you don't want to display it.",
      (WidgetTester tester) async {
    var listItemsWithTrailingNav = <DlListItem>[
      DlListItem()
    ];

    var widget = CupertinoPageScaffold(
        child: DlList(listItemsWithTrailingNav)).withCupertinoApp();
    await tester.pumpWidget(widget);
   
    bool chevronIconIsPresent = true;
    
    find.byType(Icon).evaluate().forEach((element) {
      // The Unicode code point at which this icon is stored in the icon font, same storage point means same icon!
      chevronIconIsPresent =  element.widget is Icon && (element.widget as Icon).icon!.codePoint == CupertinoIcons.chevron_right.codePoint;
    });

    expect(chevronIconIsPresent, true);
  });
}

// OnTap tests

Future<void> materialListTilesCanSetOnTapFunction() async {
  return testWidgets(
      'Material ListTiles can set the OnTap function so that the list item will respond to user when tapped.',
      (WidgetTester tester) async {
    var widget = Scaffold(body: DlList(listItems)).withMaterialApp();
    await tester.pumpWidget(widget);
    var listTile = tester.widget<ListTile>(find.byType(ListTile));
    expect(listTile.onTap, onTap);
  });
}

Future<void> cupertinoListTilesCanSetOnTapFunction() async {
  return testWidgets(
      'Cupertino ListTiles can set the OnTap function so that the list item will respond to user when tapped.',
      (WidgetTester tester) async {
    var widget = DlList(listItems).withCupertinoApp();
    await tester.pumpWidget(widget);
    var listTile =
        tester.widget<CupertinoListTile>(find.byType(CupertinoListTile));
    expect(listTile.onTap, onTap);
  });
}
