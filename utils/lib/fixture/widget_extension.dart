// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

extension WidgetExtension on Widget {
  Widget withCupertinoApp({
    CupertinoThemeData? theme,
  }) {
    theme = theme ?? const CupertinoThemeData(brightness: Brightness.light);
    var cupertinoApp = CupertinoApp(
      title: '7IM Components',
      theme: theme,
      home: this,
    );

    return cupertinoApp;
  }

  Widget withMaterialApp({
    ThemeData? theme,
  }) {
    theme = theme ?? ThemeData(brightness: Brightness.light);
    var materialApp = MaterialApp(
      title: '7IM Components',
      theme: theme,
      home: this,
    );
    return Scaffold(
        body: SizedBox(
          width: double.infinity,
          height: double.infinity,
          child: materialApp,
        ),
      );
    
    
  }
}
